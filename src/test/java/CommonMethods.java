import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.Random;

public class CommonMethods {


    // Login to prestaShop
    public static void prestaLogin(WebDriver driver){
        //Открыть страницу Админ панели
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        //Ввести логин, пароль и нажать кнопку Логин.
        //Логин: webinar.test@gmail.com
        WebElement login_form = driver.findElement(By.id("email"));
        login_form.sendKeys("webinar.test@gmail.com");
        //Пароль: Xcg7299bnSmMuRLp9ITw
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("submitLogin")));

        WebElement submit = driver.findElement(By.name("submitLogin"));
        submit.click();
    }

    // Sleep
    public static void threadSleep (int timeout){
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Driver initialization

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver();
    }


    public static String generateString()
    {
        Random rng = new Random();
        String characters = "abcdefghijklmnopqrstuvwxyz1234567890";
        int length = 15;
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String("Tov_ "+ text + generateRandomCost().toString());
    }


     /**
     * Метод получения псевдослучайного целого числа от 0 до max (включая max);
     */
    public static Integer generateRandomQty()
    {
        int min = 1, max = 100;
        return (int) (min + (int)(Math.random() * ((max - min) + 1))
        );
    }

    public static Float generateRandomCost()
    {
        int min = 1, max = 1000;
        int result = ((min + (int)(Math.random() * ((max - min) + 1))));
        float resultF = (float) result / 10;
        return (float) resultF;
    }

}
