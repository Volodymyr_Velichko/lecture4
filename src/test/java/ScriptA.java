import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class ScriptA {
    public static void main(String[] args) {
        WebDriver driver = CommonMethods.getDriver();
        driver.manage().window().maximize();


        //********************************PART*A********************************************
        //1. Войти в Админ Панель.
        CommonMethods.prestaLogin (driver);

        //add waitings
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("subtab-AdminCatalog")));

        //2. Выбрать пункт меню Каталог -> товары и дождаться загрузки страницы продуктов.
        WebElement catalog = driver.findElement(By.id("subtab-AdminCatalog"));

        Actions builder = new Actions(driver);
        builder.moveToElement(catalog).build().perform();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("товары")));

        //WebElement categories = driver.findElement(By.cssSelector("li[data-submenu='10']"));
        WebElement categories = driver.findElement(By.linkText("товары"));
        categories.click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("page-header-desc-configuration-add")));

        //3. Нажать «Новый товар» для перехода к созданию нового продукта, дождаться загрузки страницы.
        WebElement newTovarButton = driver.findElement(By.id("page-header-desc-configuration-add"));
        newTovarButton.click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("form_step1_name_1")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='submit']")));


        //4. Заполнить следующие свойства нового продукта: Название продукта,
        // Количество, Цена. Свойства продукта должны генерироваться случайно
        // (случайное название продукта, количество от 1 до 100, цена от 0.1 ₴ до 100 ₴).
        WebElement nameTovara = driver.findElement(By.id("form_step1_name_1"));
        String nameTovaraRandom = CommonMethods.generateString();
        nameTovara.sendKeys(nameTovaraRandom);

        WebElement qtyTovara = driver.findElement(By.id("form_step1_qty_0_shortcut"));
        String qtyTovaraRandom = CommonMethods.generateRandomQty().toString();
        qtyTovara.sendKeys(qtyTovaraRandom);

        WebElement costTovara = driver.findElement(By.id("form_step1_price_shortcut"));
        String costTovaraRandom = CommonMethods.generateRandomCost().toString();
        costTovara.clear();
        costTovara.sendKeys(costTovaraRandom);

        //5. После заполнения полей активировать продукт используя переключатель на
        //нижней плавающей панели. После активации продукта дождаться
        //всплывающего уведомления о сохранении настроек и закрыть его.
        //WebElement activateTovar = driver.findElement(By.id("form_step1_active"));
        WebElement activateTovar = driver.findElement(By.className("switch-input"));

        //growl growl-notice growl-medium
        //WebElement activateTovar = driver.findElement(By.xpath("//div[@class='switch-input']"));
        activateTovar.click();
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='growl']")));
        //wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Настройки обновлены.")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='growl-message']")));

        //6. Сохранить продукт нажав на кнопку «Сохранить». Дождаться всплывающего
        //уведомления о сохранении настроек и закрыть его.
        //WebElement saveButton = driver.findElement(By.xpath("//button[@type='submit'] and contains(text(),'Сохранить')"));
        //WebElement saveButton = driver.findElement(By.xpath("//button[@type='submit']//span[contains(text(), 'Сохранить')]//.."));

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".js-btn-save")));

        WebElement saveButton = driver.findElement(By.cssSelector(".js-btn-save"));
        saveButton.click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='growl-message']")));

        //********************************PART*B********************************************

        //1. Перейти на главную страницу магазина.  http://prestashop-automation.qatestlab.com.ua/
        driver.get("http://prestashop-automation.qatestlab.com.ua/");

        //2. Перейти к просмотру всех продуктов воспользовавшись ссылкой «Все товары».
        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("all-product-link")));
        WebElement allProducts = driver.findElement(By.className("all-product-link"));
        allProducts.click();

        //Добавить проверку (Assertion), что созданный в Админ Панели продукт
        //отображается на странице.

        WebElement nextButton = driver.findElement(By.xpath("//a[@rel='next']"));
        while(true){
            try{
                wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(nameTovaraRandom)));
                break;
            }
            catch (Exception e){
                System.out.println("Tovar is not found on the current page");
            }
            try{nextButton.click();}
            catch(Exception e){
                System.out.println("There is no more pages!");
                break;
            }
        }
        WebElement newTovar = driver.findElement(By.linkText(nameTovaraRandom));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(nameTovaraRandom)));
        Assert.assertEquals(newTovar.getText(), nameTovaraRandom, "New tovar was not found");
        //Assert.assertEquals(newTovar.getText(), "h"+ nameTovaraRandom, "New tovar was not found");

        //3. Открыть продукт. Добавить проверки, что название продукта, цена и количество
        //соответствует значениям, которые вводились при создании продукта в первой
        //части сценария.

        newTovar.click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(nameTovaraRandom)));
        Assert.assertEquals(driver.getTitle(), nameTovaraRandom, "Title is not correct!");
        WebElement nameOfTovar = driver.findElement(By.xpath("//h1[@class='h1']"));
        Assert.assertEquals(nameOfTovar.getText(), nameTovaraRandom.toUpperCase(), "Tovar's name is incorrect");
        WebElement costOfTovar = driver.findElement(By.xpath("//span[@itemprop='price']"));
//        String actualCost = costOfTovar.getAttribute("content");
//        System.out.println(actualCost);
//        System.out.println(costTovaraRandom);

        Assert.assertEquals(costOfTovar.getAttribute("content"), costTovaraRandom, "Cost of tovar is unexpected");
        String tovars = qtyTovaraRandom + " Товары";
        WebElement qtyOfTovar = driver.findElement(By.xpath("//span[text()="+ "'" + tovars + "']"));
        Assert.assertEquals(qtyOfTovar.getText(), qtyTovaraRandom + " Товары", "Amount of tovar is unexpected");

//        //После входа в систему нажать на пиктограмме пользователя в верхнем правом углу и выбрать опцию «Выход.»
//        WebElement profile_button = driver.findElement(By.id("employee_infos"));
//        profile_button.click();
//
//        WebElement logout = driver.findElement(By.id("header_logout"));
//        logout.click();

        driver.quit();
    }
}
